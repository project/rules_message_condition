<?php

/**
 * Implements hook_rules_condition_info().
 */
function rules_message_condition_rules_condition_info() {
  return array(
    'rules_message_condition_rendered_compare' => array(
      'label' => t('Compare rendered message'),
      'arguments' => array(
        'message' => array(
          'label' => 'Message1',
          'type' => 'message',
          'description' => 'The source message to compare with.',
        ),
        'message_compare' => array(
          'label' => 'Message2',
          'type' => 'message',
          'description' => 'The target message to compare against.',
        ),
        'compare_delta' => array(
          'label' => t('Compare on specific rendered text delta.'),
          'description' => t('Compare the rendered text of the whole message, or a specific delta.'),
          'type' => 'integer',
          'allow null' => TRUE,
          'optional' => TRUE,
        ),
      ),
      'callbacks' => array(
        'form_alter' => 'rules_message_condition_rendered_compare_form_alter',
      ),
      'group' => 'entities'
    ),
  );
}

function rules_message_condition_rendered_compare($entity1, $entity2, $delta = null) {
  $options = $delta == null
    ? array()
    : array(
      'partials'=> 1,
      'partial delta' => $delta
    );
  $left_text = $entity1->getText(LANGUAGE_NONE, $options);
  $right_text = $entity2->getText(LANGUAGE_NONE, $options);

  return $left_text == $right_text;
}

function rules_message_condition_rendered_compare_form_alter(&$form, &$form_state, $options, RulesAbstractPlugin $element) {
  $form['parameter']['compare_delta']['settings']['compare_delta']['#title'] = 'Delta';
  $form['parameter']['compare_delta']['settings']['compare_delta']['#prefix'] = '<div><i>' . t('Leave blank to compare on whole rendered text.') . '</i></div>';

}
